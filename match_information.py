import requests
import time

def get_match_info(match_id, api_key):
    """Does the API call for match info"""
    match_url = 'https://euw1.api.riotgames.com/lol/match/v4/matches/'
    parameters = {'api_key': api_key}

    req = '%s%s' % (match_url, match_id)

    time.sleep(1/20)

    response = requests.get(req, params=parameters)

    if response.status_code == 200:
        return response.json()
    else:
        return response.status_code


def get_match_timeline(match_id, api_key):
    """Does the API call for match timeline"""
    match_url = 'https://euw1.api.riotgames.com/lol/match/v4/timelines/by-match/'
    parameters = {'api_key': api_key}

    req = '%s%s' % (match_url, match_id)

    response = requests.get(req, params=parameters)

    if response.status_code == 200:
        response_edited = response.json()
        response_edited.update({'matchId': match_id})
        return response_edited
    else:
        return response.status_code
