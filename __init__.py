from .match_information import get_match_info
from .match_information import get_match_timeline

from .match_processing import make_match_units
from .match_processing import make_participantId_mapper
from .match_processing import make_item_list
from .match_processing import make_kill_list
from .match_processing import make_bigmonsterkill_list
from .match_processing import make_bans
from .match_processing import make_picks
from .match_processing import make_pickban

def manual_testing_function(n):
    for i in range(n):
        print(i)