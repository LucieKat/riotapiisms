### Player-based match information
def make_match_units(response):
    """
    This function outputs a bare object: for every match, for every participant, a bunch of statistics.
    It can be used to draw conclusions on individual units in the game: players and champions primarily.
    :param response: The json object out of an API call to the match API
    :return: A list of dictionaries, every dictionary for an entry per participant per match.
    """

    if type(response) == int: # tests whether the response is the api error code or actual output
        return 'Faulty response!'

    # The stuff we want in every line
    matchId = response['gameId']
    patch = response['gameVersion']
    duration = response['gameDuration']
    # The object that will hold all participants
    entries = []

    for participant in response['participants']:
        participantId = participant['participantId']
        champion = participant['championId']
        win = participant['stats']['win']
        if participant['teamId'] == 100:
            side = 'blue'
        elif participant['teamId'] == 200:
            side = 'red'
        kills, deaths, assists = participant['stats']['kills'], participant['stats']['deaths'], participant['stats']['assists']
        totalDamageDealt = participant['stats']['totalDamageDealt']
        totalChampDamage = participant['stats']['totalDamageDealtToChampions']
        sCC = participant['stats']['timeCCingOthers']
        visionScore = participant['stats']['visionScore']
        totalDamageTaken = participant['stats']['totalDamageTaken']
        goldEarned = participant['stats']['goldEarned']
        minionsKilled = participant['stats']['totalMinionsKilled']
        neutralMinionsKilled = participant['stats']['neutralMinionsKilled']
        firstBloodKill, firstBloodAssist = participant['stats']['firstBloodKill'], participant['stats']['firstBloodAssist']

        spell_D = participant['spell1Id']
        spell_F = participant['spell2Id']

        entry = {
            'matchId': matchId, 'patch': patch, 'duration': duration,
            'participantId': participantId,
            'champId': champion,
            'win': win,
            'side': side,
            # 'lane': lane,
            'kills': kills, 'deaths': deaths, 'assists': assists,
            'cs': minionsKilled+neutralMinionsKilled, 'neutralMinionsKilled': neutralMinionsKilled, 'minionsKilled': minionsKilled,
            'goldEarned': goldEarned, 'visionScore': visionScore,
            'damageDealt': totalDamageDealt, 'damageDealtToChampions': totalChampDamage, 'damageTaken': totalDamageTaken,
            'sCC': sCC,
            'firstBloodKill': firstBloodKill, 'firstBloodAssist': firstBloodAssist,
            'spell_D': spell_D, 'spell_F': spell_F
        }

        entries.append(entry)

    return entries

### Get a mapper of matchId and participartId and win/lost
def make_participantId_mapper(response):
    """
    This function outputs a wrapper to link participantId and matchId to team+player Id.
    :param response: The json object out of an API call to the match API
    :return: A list of dictionaries, every dictionary per participant per match.
    """

    if type(response) == int: # tests whether the response is the api error code or actual output
        return 'Faulty response!'

    participants = []

    matchId = response['gameId']
    for participant in response['participants']:
        participantId = participant['participantId']
        win = participant['stats']['win']
        champion = participant['championId']

        lane = 'NA'
        if participantId == 1 or participantId == 6:
            lane = 'Top'
        elif participantId == 2 or participantId == 7:
            lane = 'Jungle'
        elif participantId == 3 or participantId == 8:
            lane = 'Mid'
        elif participantId == 4 or participantId == 9:
            lane = 'ADC'
        elif participantId == 5 or participantId == 10:
            lane = 'Support'

        participant = {
            'matchId': matchId,
            'participantId': participantId,
            'win': win,
            'lane': lane,
            'champId': champion
        }

        participants.append(participant)

    return participants

####-- Timeline --####
### Items
def make_item_list(response):
    """
    Outputs a list of all items bought in the match,
    :param response: Response to an API call to the timeline endpoint
    :return: List of all items bought in game, with time stamp, participant ID and item ID
    """
    items = []

    for frame in response['frames']:
        for event in frame['events']:
            if event['type'] == 'ITEM_PURCHASED':
                # print(event['itemId'])
                items.append({
                    'matchId': response['matchId'],
                    'timestamp': event['timestamp'],
                    'participantId': event['participantId'],
                    'itemId': event['itemId']
                })
    return items

### Kills
def make_kill_list(response):
    """
    Outputs a list of all kills made in a match,
    :param response: Response to an API call to the timeline endpoint
    :return: List of all kills made in game, with time stamp, killedId, victimId, assistantIds, and position (x, y)
    """
    kills = []

    for frame in response['frames']:
        for event in frame['events']:
            if event['type'] == 'CHAMPION_KILL':
                # print(event)
                kills.append({
                    'matchId': response['matchId'],
                    'timestamp': event['timestamp'],
                    'killerId': event['killerId'],
                    'victimId': event['victimId'],
                    'assistIds': event['assistingParticipantIds'],
                    'x': event['position']['x'],
                    'y': event['position']['y']
                })
    return kills

### Big monsters
def make_bigmonsterkill_list(response):
    """
    Outputs a list of all big monsters killed in the match,
    :param response: Response to an API call to the timeline endpoint
    :return: List of all big monsters killed in game, with time stamp, participant ID and monster name
    """
    monsters = []

    for frame in response['frames']:
        for event in frame['events']:
            if event['type'] == 'ELITE_MONSTER_KILL':
                monsterSubType = 'NA'
                if event['monsterType'] == 'DRAGON':
                    monsterSubType = event['monsterSubType']
                monsters.append({
                    'matchId': response['matchId'],
                    'timestamp': event['timestamp'],
                    'killerId': event['killerId'],
                    'monsterType': event['monsterType'],
                    'monsterSubType': monsterSubType,
                    'x': event['position']['x'],
                    'y': event['position']['y']
                })
    return monsters

####-- Pick/bans --####
def make_bans(response):
    matchId = response['gameId']
    bans = []
    for teams in response['teams']:
        for ban in teams['bans']:
            bans.append(
                {
                    'matchId': matchId,
                    'type': 'ban',
                    'champId': ban['championId'],
                    'pickTurn': ban['pickTurn'],
                    'participantId': 'NA'
                }
            )
    return bans

def make_picks(response):
    matchId = response['gameId']
    picks = []
    for participant in response['participants']:
        picks.append(
            {
                'matchId': matchId,
                'type': 'pick',
                'champId': participant['championId'],
                'pickTurn': 'NA',
                'participantId': participant['participantId']
            }
        )
    return picks

def make_pickban(response):
    bans = make_bans(response)
    picks = make_picks(response)
    pickban = [*bans, *picks]
    return(pickban)

